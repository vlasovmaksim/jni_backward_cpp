#include <jni.h>

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_Main_hello
  (JNIEnv *, jclass);

#ifdef __cplusplus
}
#endif
