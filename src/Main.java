import java.lang.reflect.Field;

public class Main {

    static {
        loadLibrary();
    }    

    private static native void hello();


    public static void main(String[] args) {
        System.out.println("Hello World from Java!");

        hello();
    }

    public static void loadLibrary() {
        // Get project directory.
        String projectDir = System.getProperty("user.dir");
        System.out.println("Project directory: " + projectDir);

        // Set library path.
        String libraryDir = projectDir + "/lib";
        System.out.println("Library directory: " + libraryDir);

        // Add library path to the system libraries paths.
        System.setProperty("java.library.path", libraryDir);


        // Links:
//        http://blog.cedarsoft.com/2010/11/setting-java-library-path-programmatically/
//        http://stackoverflow.com/questions/5419039/is-djava-library-path-equivalent-to-system-setpropertyjava-library-path
        Field fieldSysPath = null;
        try {
            fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        assert fieldSysPath != null;
        fieldSysPath.setAccessible(true);

        try {
            fieldSysPath.set(null, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // Print the system libraries paths.
        System.out.println(System.getProperty("java.library.path"));

        // Load the library.
        try {
            System.loadLibrary("hello");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Lbrary failed to load.\n" + e);
            System.exit(1);
        }
    }
}
